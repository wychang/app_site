"""mblog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django import urls
from django.contrib import admin
from django.urls import path, re_path, include
from mainsite.views import * 
import singup.urls, video.urls
urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^$',homepage),
    path('post/<slug:slug>',showpost),
    path('author/',author, {'author_id':'paza19870000'}),
    path('author/<str:author_id>', author),
    path('singup/', include(singup.urls) ),
    path('video/', include(video.urls)),
    ########
    path('post-time/<int:yr>/<int:mon>/<int:day>/<int:post_num>', post_times, name='post-url'),
     
]
